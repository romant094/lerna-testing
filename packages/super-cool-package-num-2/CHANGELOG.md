# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.4](https://gitlab.com/romant094/lerna-testing/compare/super-cool-package-num-2@1.0.3...super-cool-package-num-2@1.0.4) (2021-10-12)

**Note:** Version bump only for package super-cool-package-num-2
