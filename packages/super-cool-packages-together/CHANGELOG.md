# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.4](https://gitlab.com/romant094/lerna-testing/compare/super-cool-packages-together@1.1.3...super-cool-packages-together@1.1.4) (2021-10-12)

**Note:** Version bump only for package super-cool-packages-together
